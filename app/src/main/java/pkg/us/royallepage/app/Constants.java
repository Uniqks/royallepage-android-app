package pkg.us.royallepage.app;

/**
 * Created by Seema on 30/10/18.
 */

public class Constants {

    public static int MEHTOD_POST = 1;
    public static int MEHTOD_GET = 2;

    public static final String FAILURE = "0";
    public static final String SUCCESS = "1";

    public static final String PREF_USER_PROFILE = "UserProfile";

    public  static final String BASE_URL = "http://tradetrack.com.au/json/";
    public  static final String IMAGE_URL = "http://tradetrack.com.au/uploads/";

    public static boolean isAlertDialogShown = false;

    public static String IMAGE_PATH = "TradeTrack/Media/Images";


    public static String CLASS = "class";

}
