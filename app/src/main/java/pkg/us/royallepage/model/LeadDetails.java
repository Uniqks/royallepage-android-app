package pkg.us.royallepage.model;

import java.io.Serializable;

/**
 * Created by Seema on 2/11/18.
 */

public class LeadDetails implements Serializable {

    private String uName;
    private String email;
    private String phone;
    private String city;
    private String date;
    private String status;

    public LeadDetails(String uName, String email, String phone, String city, String date, String status) {
        this.uName = uName;
        this.email = email;
        this.phone = phone;
        this.city = city;
        this.date = date;
        this.status = status;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
