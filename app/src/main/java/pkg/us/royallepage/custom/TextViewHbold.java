package pkg.us.royallepage.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Seema on 1/11/18.
 */

public class TextViewHbold extends TextView {

    public TextViewHbold(Context context) {
        super(context);
        init();
    }

    public TextViewHbold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewHbold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/ubuntu_dbold.ttf"));
        }
    }
}