package pkg.us.royallepage.activity.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pkg.us.royallepage.R;
import pkg.us.royallepage.activity.adapter.FragmentTabsPagerAdapter;


public class ChatFragment extends Fragment {


    private View mView;
    public TabLayout mChatTab;
    public ViewPager mViewPager;
    private FragmentTabsPagerAdapter mPagerAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_chat, container, false);





        initControls();
        return mView;
    }

    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {

        mChatTab = mView.findViewById(R.id.chat_tab);
        mViewPager = mView.findViewById(R.id.viewPager);
        setupViewPager(mViewPager);
    }



    /**
     *  setup view pager
     */
    private void setupViewPager(ViewPager viewPager) {
        mPagerAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());

        ChatActiveFragment activeFragment = new ChatActiveFragment();
        mPagerAdapter.addFragment(activeFragment, getResources().getString(R.string.Cactive));


        viewPager.setAdapter(mPagerAdapter);
        mChatTab.setupWithViewPager(viewPager);
        mChatTab.setTabMode(1);
    }




}
