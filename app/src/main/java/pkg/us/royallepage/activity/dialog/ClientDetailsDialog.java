package pkg.us.royallepage.activity.dialog;


import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import pkg.us.royallepage.R;
import pkg.us.royallepage.app.Constants;
import pkg.us.royallepage.databinding.DialogClientDetailsBinding;


public class ClientDetailsDialog extends DialogFragment implements View.OnClickListener {


    DialogClientDetailsBinding binding;

    private String mGetClass;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_client_details, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.enterDialogAnimation;

        if (getArguments() != null) {
            mGetClass = getArguments().getString(Constants.CLASS);
        }





        initControls();
        return binding.getRoot();
    }


    /**
     * Method is used to initialized all the controls.....
     */
    private void initControls() {

        binding.closeRL.setOnClickListener(this);


        if (mGetClass.equals("active")) {
            binding.statusTV.setText("Active");
        } else {
            binding.statusTV.setText("Inactive");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.closeRL:
                getDialog().getWindow().getAttributes().windowAnimations = R.style.outDialogAnimation;
                getDialog().dismiss();
                break;
        }
    }
}
