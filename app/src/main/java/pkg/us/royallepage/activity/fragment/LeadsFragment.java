package pkg.us.royallepage.activity.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pkg.us.royallepage.R;
import pkg.us.royallepage.activity.adapter.FragmentTabsPagerAdapter;
import pkg.us.royallepage.activity.interfaces.onBackPressed;


public class LeadsFragment extends Fragment {


    private View mView;
    public TabLayout mLeadsTab;
    public ViewPager mViewPager;
    private FragmentTabsPagerAdapter mPagerAdapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_leads, container, false);






        initControls();
        return mView;
    }


    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {

        mLeadsTab = mView.findViewById(R.id.leads_tab);
        mViewPager = mView.findViewById(R.id.viewPager);
        setupViewPager(mViewPager);
    }



    /**
     *  setup view pager
     */
    private void setupViewPager(ViewPager viewPager) {
        mPagerAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());

        NewLeadFragment newLeadFragment = new NewLeadFragment();
        mPagerAdapter.addFragment(newLeadFragment, getResources().getString(R.string.newLead));

        ActiveLeadFragment activeLeadFragment = new ActiveLeadFragment();
        mPagerAdapter.addFragment(activeLeadFragment, getResources().getString(R.string.activeLead));



        viewPager.setAdapter(mPagerAdapter);
        mLeadsTab.setTabMode(TabLayout.MODE_SCROLLABLE);
        mLeadsTab.setupWithViewPager(viewPager);
        mLeadsTab.setTabMode(1);
    }



}
