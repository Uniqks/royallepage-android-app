package pkg.us.royallepage.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pkg.us.royallepage.R;
import pkg.us.royallepage.databinding.FragmentActiveLeadBinding;
import pkg.us.royallepage.model.LeadDetails;


public class ActiveLeadFragment extends Fragment {


    FragmentActiveLeadBinding binding;

    private List<LeadDetails> mLeadDetails = new ArrayList<>();
    private LeadListAdapter mLeadAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_active_lead, container, false);







        initControls();
        setLeadListAdapter();
        return binding.getRoot();
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

    }


    /**
     * Method is used to set set up lead list...
     */
    private void setLeadListAdapter() {

        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Canada Landing"));
        this.mLeadDetails.add(new LeadDetails("John", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "19 Oct, 2018 7:03 pm", "Ontario Landing"));
        this.mLeadDetails.add(new LeadDetails("Sia Gracee", "frankdigitally@gmail.com", "1-858-969-6328", "Barrie", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Toronto Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "ThornHill", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "International Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Mississauga", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Canada Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Canada Landing"));
        this.mLeadDetails.add(new LeadDetails("John", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "19 Oct, 2018 7:03 pm", "Ontario Landing"));
        this.mLeadDetails.add(new LeadDetails("Sia Gracee", "frankdigitally@gmail.com", "1-858-969-6328", "Barrie", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Toronto Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "ThornHill", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Milton", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "London", "2 Nov, 2018 10:01 am", "Live BookShow"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "London", "2 Nov, 2018 10:01 am", "Live Landing   "));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Selby", "2 Nov, 2018 10:01 am", "International Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "WaterLoo", "2 Nov, 2018 10:01 am", "Canada Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Canada Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "London", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "ThornHill", "2 Nov, 2018 10:01 am", "Ontario Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Mississauga", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Canada Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Milton", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "London", "2 Nov, 2018 10:01 am", "Live BookShow"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Canada Landing"));
        this.mLeadDetails.add(new LeadDetails("John", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "19 Oct, 2018 7:03 pm", "Ontario Landing"));
        this.mLeadDetails.add(new LeadDetails("Sia Gracee", "frankdigitally@gmail.com", "1-858-969-6328", "Barrie", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Toronto Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "Toronto", "2 Nov, 2018 10:01 am", "Live Landing"));
        this.mLeadDetails.add(new LeadDetails("Frank Nava", "frankdigitally@gmail.com", "1-858-969-6328", "ThornHill", "2 Nov, 2018 10:01 am", "Live Landing"));

        mLeadAdapter = new LeadListAdapter(mLeadDetails, ActiveLeadFragment.this);
        binding.leadList.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.leadList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.list_from_bottom));
        binding.leadList.setAdapter(mLeadAdapter);
    }



    /**
     *  new Lead list adapater...
     */
    public class LeadListAdapter extends RecyclerView.Adapter<LeadListAdapter.ViewHolder> {

        private List<LeadDetails> mData;
        private Fragment fragment;

        public LeadListAdapter(List<LeadDetails> mData, Fragment fragment) {
            this.mData = mData;
            this.fragment = fragment;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_lead_list, viewGroup, false);

            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            if (mData.get(position).getuName() != null && !mData.get(position).getuName().equals("")) {
                holder.mUnameTxt.setText(mData.get(position).getuName());
            } else {
            }

            if (mData.get(position).getEmail() != null && !mData.get(position).getEmail().equals("")) {
                holder.mEmailTxt.setText(mData.get(position).getEmail());
            } else {
            }

            if (mData.get(position).getPhone() != null && !mData.get(position).getPhone().equals("")) {
                holder.mPhoneTxt.setText(mData.get(position).getPhone());
            } else {
            }

            if (mData.get(position).getCity() != null && !mData.get(position).getCity().equals("")) {
                holder.mCityTxt.setText(mData.get(position).getCity());
            } else {
            }


            if (mData.get(position).getDate() != null && !mData.get(position).getDate().equals("")) {
                holder.mDateTxt.setText(mData.get(position).getDate());
            } else {
            }

            if (mData.get(position).getStatus() != null && !mData.get(position).getStatus().equals("")) {
                holder.mStatusTxt.setText(mData.get(position).getStatus());
            } else {
            }


        }

        @Override
        public int getItemCount() {
            return mData.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView mUnameTxt, mEmailTxt, mPhoneTxt, mCityTxt, mDateTxt, mStatusTxt;
            LinearLayout mArstatusLl;

            public ViewHolder(@NonNull View view) {
                super(view);

                mUnameTxt = view.findViewById(R.id.txt_uname);
                mEmailTxt = view.findViewById(R.id.txt_email);
                mPhoneTxt = view.findViewById(R.id.txt_phone);
                mCityTxt = view.findViewById(R.id.txt_city);
                mDateTxt = view.findViewById(R.id.txt_date);
                mStatusTxt = view.findViewById(R.id.txt_status);
                mArstatusLl = view.findViewById(R.id.ll_arstatus);
                mArstatusLl.setVisibility(View.GONE);
            }
        }
    }


}
