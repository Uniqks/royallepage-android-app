package pkg.us.royallepage.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pkg.us.royallepage.R;
import pkg.us.royallepage.activity.dialog.ClientDetailsDialog;
import pkg.us.royallepage.app.Constants;
import pkg.us.royallepage.databinding.FragmentClientActiveBinding;
import pkg.us.royallepage.model.ClientDetails;
import pkg.us.royallepage.model.LeadDetails;


public class ClientActiveFragment extends Fragment {


    FragmentClientActiveBinding binding;

    private List<ClientDetails> mClientDetails = new ArrayList<>();
    private ClientListAdapter mClientListAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_active, container, false);






        initControls();
        setClientListAdapter();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

    }


    /**
     *  Method is used to set client list adapter..
     */
    private void setClientListAdapter() {

        this.mClientDetails.add(new ClientDetails("Frank Nava", "Frank", "Nava", "softensoftwares@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Hitesh Prajapati", "Hitesh", "Prajapati", "hitesh@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Sia Gracee", "Sia", "Gracee", "sia@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank ", "Frank", "Nava", "frank@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank Tester 1", "Frank", "Nava", "tester@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("John", "John", "", "john@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Steve Murria", "Steve", "Murria", "steve@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank Nava", "Frank", "Nava", "softensoftwares@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Hitesh Prajapati", "Hitesh", "Prajapati", "hitesh@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Sia Gracee", "Sia", "Gracee", "sia@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank ", "Frank", "Nava", "frank@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank Tester 1", "Frank", "Nava", "tester@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("John", "John", "", "john@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Steve Murria", "Steve", "Murria", "steve@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank Nava", "Frank", "Nava", "softensoftwares@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Hitesh Prajapati", "Hitesh", "Prajapati", "hitesh@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Sia Gracee", "Sia", "Gracee", "sia@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank ", "Frank", "Nava", "frank@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank Tester 1", "Frank", "Nava", "tester@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("John", "John", "", "john@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Steve Murria", "Steve", "Murria", "steve@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank Nava", "Frank", "Nava", "softensoftwares@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Hitesh Prajapati", "Hitesh", "Prajapati", "hitesh@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Sia Gracee", "Sia", "Gracee", "sia@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank ", "Frank", "Nava", "frank@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Frank Tester 1", "Frank", "Nava", "tester@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("John", "John", "", "john@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));
        this.mClientDetails.add(new ClientDetails("Steve Murria", "Steve", "Murria", "steve@gmail.com","1-858-969-6328", "1200 Bay st, Toronto, Ontorio", "A trusted real estate...","2 Nov, 2018"));


        mClientListAdapter = new ClientListAdapter(mClientDetails, ClientActiveFragment.this);
        binding.clientList.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.clientList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.list_from_bottom));
        binding.clientList.setAdapter(mClientListAdapter);
    }



    /**
     * Client list adapter...
     */
    public class ClientListAdapter extends RecyclerView.Adapter<ClientListAdapter.ViewHolder> {

        private List<ClientDetails> mData;
        private Fragment fragment;

        public ClientListAdapter(List<ClientDetails> mData, Fragment fragment) {
            this.mData = mData;
            this.fragment = fragment;
        }


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_client_list, viewGroup, false);


            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            if (mData.get(position).getuName() != null && !mData.get(position).getuName().equals("")) {
                holder.mUnameTxt.setText(mData.get(position).getuName());

                String title = mData.get(position).getuName().toUpperCase();
                if (title.length() > 0) {
                    holder.mLableTxt.setText(title.substring(0, 1).toUpperCase());
                } else {
                    holder.mLableTxt.setText("T");
                }
            } else {
            }

            if (mData.get(position).getDescription() != null && !mData.get(position).getDescription().equals("")) {
                holder.mDescTxt.setText(mData.get(position).getDescription());
            } else {
            }

            if (mData.get(position).getDate() != null && !mData.get(position).getDate().equals("")) {
                holder.mDateTxt.setText(mData.get(position).getDate());
            } else {
            }


            holder.mMainLl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ClientDetailsDialog dialog = new ClientDetailsDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.CLASS, "active");
                    dialog.setArguments(bundle);
                    dialog.show(getFragmentManager(), "Client Details");
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView mUnameTxt, mDateTxt, mDescTxt, mLableTxt;
            LinearLayout mMainLl;

            public ViewHolder(@NonNull View view) {
                super(view);

                mUnameTxt = view.findViewById(R.id.txt_uname);
                mDateTxt = view.findViewById(R.id.txt_date);
                mDescTxt = view.findViewById(R.id.txt_desc);
                mLableTxt = view.findViewById(R.id.txt_lable);

                mMainLl = view.findViewById(R.id.ll_main);
            }
        }
    }


}
