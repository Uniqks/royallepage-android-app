package pkg.us.royallepage.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.us.royallepage.R;
import pkg.us.royallepage.app.RoyalApp;
import pkg.us.royallepage.databinding.ActivityRegisterBinding;
import spencerstudios.com.bungeelib.Bungee;

public class Register extends AppCompatActivity implements View.OnClickListener {


    ActivityRegisterBinding binding;
    private RoyalApp mRoyalApp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        mRoyalApp = (RoyalApp) getApplication();





        initControls();
    }

    /**
     *  Method is used to initialized all the controls..
     */
    private void initControls() {

        binding.topRL.setVisibility(View.GONE);
        setAnimationView(binding.topRL);

        binding.registerRL.setOnClickListener(this);
        binding.loginLL.setOnClickListener(this);
    }


    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainTopLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        mMainTopLl.startAnimation(bottomUp);
        mMainTopLl.setVisibility(View.VISIBLE);
    }

    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mRoyalApp.isValidUName(binding.unameET)) {
            return false;
        }
        if (!mRoyalApp.isValidEmail(binding.emailET)) {
            return false;
        }
        if (!mRoyalApp.isValidUName(binding.mobileET)) {
            return false;
        }
        if (!mRoyalApp.isValidatePassword(binding.pwdET)) {
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.registerRL:
                mRoyalApp.hideKeyboard(Register.this);
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.registerRL));

                if (checkValidation()){

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(Register.this, "Work in progress....", Toast.LENGTH_SHORT).show();
                            Bungee.slideLeft(Register.this);
                        }
                    }, 200);
                }
                break;

            case R.id.loginLL:
                mRoyalApp.hideKeyboard(Register.this);
                startActivity(new Intent(Register.this, Login.class));
                finish();
                Bungee.slideLeft(Register.this);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Register.this, Login.class));
        finish();
        Bungee.slideLeft(Register.this);
    }
}
