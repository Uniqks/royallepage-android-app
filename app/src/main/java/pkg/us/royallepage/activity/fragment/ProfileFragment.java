package pkg.us.royallepage.activity.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import pkg.us.royallepage.R;
import pkg.us.royallepage.activity.dialog.ChangePwdDialog;
import pkg.us.royallepage.activity.dialog.EditProfileDialog;
import pkg.us.royallepage.activity.interfaces.onBackPressed;
import pkg.us.royallepage.databinding.FragmentProfileBinding;


public class ProfileFragment extends Fragment implements onBackPressed, View.OnClickListener {


    FragmentProfileBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);





        initControls();
        return binding.getRoot();
    }


    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.topLL.setVisibility(View.GONE);
        setAnimationView(binding.topLL);

        binding.backIV.setOnClickListener(this);
        binding.selectPhotoRL.setOnClickListener(this);
        binding.updateRL.setOnClickListener(this);
        binding.changePwdLL.setOnClickListener(this);
    }

    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainTopLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left_right);
        mMainTopLl.startAnimation(bottomUp);
        mMainTopLl.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIV:
                hideKey();
                getActivity().getSupportFragmentManager().popBackStack();
                break;

            case R.id.changePwdLL:
                hideKey();
                ChangePwdDialog pwdDialog = new ChangePwdDialog();
                pwdDialog.show(getFragmentManager(), "Change Pwd");
                break;

            case R.id.selectPhotoRL:
                hideKey();
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                break;

            case R.id.updateRL:
                hideKey();
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /*  hide keyboard  */
    private void hideKey() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
