package pkg.us.royallepage.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import pkg.us.royallepage.R;
import spencerstudios.com.bungeelib.Bungee;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);





        StartAnimations();
        setTimeOut();
    }


    /**
     *   Method is used to set time out...
     */
    private void setTimeOut() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                try {

                    startActivity(new Intent(Splash.this, Login.class));
                    finish();
                    Bungee.slideLeft(Splash.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 2400);
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
       /* anim.reset();
        ImageView rl = findViewById(R.id.img_logoa);
        rl.clearAnimation();
        rl.startAnimation(anim);*/

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        CardView iv = findViewById(R.id.card_view);
        iv.clearAnimation();
        iv.startAnimation(anim);
    }

}

