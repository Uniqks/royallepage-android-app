package pkg.us.royallepage.activity.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pkg.us.royallepage.R;


public class AssignedLeadFragment extends Fragment {


    private View mView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_assigned_lead, container, false);






        initControls();
        return mView;
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

    }


}
