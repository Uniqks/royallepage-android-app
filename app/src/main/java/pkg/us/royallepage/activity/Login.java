package pkg.us.royallepage.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.us.royallepage.R;
import pkg.us.royallepage.activity.viewActivity.HomeActivity;
import pkg.us.royallepage.app.RoyalApp;
import pkg.us.royallepage.databinding.ActivityLoginBinding;
import spencerstudios.com.bungeelib.Bungee;

public class Login extends AppCompatActivity implements View.OnClickListener {


    ActivityLoginBinding binding;
    private RoyalApp mRoyalApp;

    boolean doubleBackToExitPressedOnce = false;

    private SharedPreferences mRememberMePref;
    private SharedPreferences.Editor mEditor;
    boolean saveLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mRoyalApp = (RoyalApp) getApplication();




        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.topRL.setVisibility(View.GONE);
        setAnimationView(binding.topRL);

        binding.loginBTN.setOnClickListener(this);
        binding.registerLL.setOnClickListener(this);
        binding.rememberCHK.setOnClickListener(this);
        binding.fpwdTV.setOnClickListener(this);

        mRememberMePref = getSharedPreferences("RememberMePref", 0);
        mEditor = mRememberMePref.edit();
        saveLogin = mRememberMePref.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            binding.emailET.setText(mRememberMePref.getString("username", ""));
            binding.pwdET.setText(mRememberMePref.getString("password", ""));
            binding.rememberCHK.setChecked(true);
        }
    }

    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainTopLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        mMainTopLl.startAnimation(bottomUp);
        mMainTopLl.setVisibility(View.VISIBLE);
    }

    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mRoyalApp.isValidEmail(binding.emailET)) {
            return false;
        }
        if (!mRoyalApp.isValidatePassword(binding.pwdET)) {
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.loginBTN:
                mRoyalApp.hideKeyboard(Login.this);
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.loginBTN));

                if (checkValidation()){

                    if (binding.rememberCHK.isChecked()) {
                        mEditor.putBoolean("saveLogin", true);
                        mEditor.putString("username", binding.emailET.getText().toString());
                        mEditor.putString("password", binding.pwdET.getText().toString());
                        mEditor.commit();
                    } else {
                        mEditor.clear();
                        mEditor.commit();
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(Login.this, HomeActivity.class));
                            finish();
                            Bungee.slideLeft(Login.this);
                        }
                    }, 200);
                }
                break;

            case R.id.registerLL:
                mRoyalApp.hideKeyboard(Login.this);
                startActivity(new Intent(Login.this, Register.class));
                finish();
                Bungee.slideLeft(Login.this);
                break;

            case R.id.rememberCHK:
                mRoyalApp.hideKeyboard(Login.this);
                break;

            case R.id.fpwdTV:
                mRoyalApp.hideKeyboard(Login.this);
                startActivity(new Intent(Login.this, ForgotPwd.class));
                finish();
                Bungee.slideLeft(Login.this);
                break;
        }
    }

    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again to close app", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;

            }
        }, 2000);
    }
}
