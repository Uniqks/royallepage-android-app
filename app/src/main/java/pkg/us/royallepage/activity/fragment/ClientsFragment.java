package pkg.us.royallepage.activity.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pkg.us.royallepage.R;
import pkg.us.royallepage.activity.adapter.FragmentTabsPagerAdapter;

public class ClientsFragment extends Fragment {


    private View mView;
    public TabLayout mClientsTab;
    public ViewPager mViewPager;
    private FragmentTabsPagerAdapter mPagerAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_clients, container, false);






        initControls();
        return mView;
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mClientsTab = mView.findViewById(R.id.clients_tab);
        mViewPager = mView.findViewById(R.id.viewPager);
        setupViewPager(mViewPager);
    }


    /**
     *  setup view pager
     */
    private void setupViewPager(ViewPager viewPager) {
        mPagerAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());

        ClientActiveFragment activeFragment = new ClientActiveFragment();
        mPagerAdapter.addFragment(activeFragment, getResources().getString(R.string.Cactive));

        ClientInactiveFragment inactiveFragment = new ClientInactiveFragment();
        mPagerAdapter.addFragment(inactiveFragment, getResources().getString(R.string.Cinactive/*, new Object[]{String.valueOf(5)}*/));

        viewPager.setAdapter(mPagerAdapter);
        mClientsTab.setupWithViewPager(viewPager);
        mClientsTab.setTabMode(1);
    }


}
