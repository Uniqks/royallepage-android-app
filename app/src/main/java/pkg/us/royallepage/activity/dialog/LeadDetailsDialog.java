package pkg.us.royallepage.activity.dialog;


import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import pkg.us.royallepage.R;
import pkg.us.royallepage.databinding.DialogLeadDetailsBinding;


public class LeadDetailsDialog extends DialogFragment implements View.OnClickListener {


    DialogLeadDetailsBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_lead_details, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.enterDialogAnimation;






        initControls();
        return binding.getRoot();
    }


    /**
     * Method is used to initialized all the controls..
     */
    private void initControls() {

        binding.closeRL.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.closeRL:
                getDialog().getWindow().getAttributes().windowAnimations = R.style.outDialogAnimation;
                getDialog().dismiss();
                break;
        }
    }
}
