package pkg.us.royallepage.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.us.royallepage.R;
import pkg.us.royallepage.activity.viewActivity.HomeActivity;
import pkg.us.royallepage.app.RoyalApp;
import pkg.us.royallepage.databinding.ActivityForgotPwdBinding;
import spencerstudios.com.bungeelib.Bungee;

public class ForgotPwd extends AppCompatActivity implements View.OnClickListener {


    ActivityForgotPwdBinding binding;
    private RoyalApp mRoyalApp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_pwd);
        mRoyalApp = (RoyalApp) getApplication();



        initControls();
    }


    /**
     *  Method is used to initialized all the controls....
     */
    private void initControls() {

        binding.topRL.setVisibility(View.GONE);
        setAnimationView(binding.topRL);

        binding.sendBTN.setOnClickListener(this);
    }


    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainTopLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        mMainTopLl.startAnimation(bottomUp);
        mMainTopLl.setVisibility(View.VISIBLE);
    }

    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mRoyalApp.isValidEmail(binding.emailET)) {
            return false;
        }
        return true;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.sendBTN:
                mRoyalApp.hideKeyboard(ForgotPwd.this);
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.sendBTN));

                if (checkValidation()){

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.emailET.setText("");
                            Bungee.slideLeft(ForgotPwd.this);
                        }
                    }, 200);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ForgotPwd.this, Login.class));
        finish();
        Bungee.slideLeft(ForgotPwd.this);
    }
}
