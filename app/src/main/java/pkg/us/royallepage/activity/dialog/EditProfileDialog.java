package pkg.us.royallepage.activity.dialog;


import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import pkg.us.royallepage.R;


public class EditProfileDialog extends DialogFragment implements View.OnClickListener {


    private View mView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.dialog_edit_profile, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);





        initControls();
        return mView;
    }


    /**
     * Method is used to initialized all the controls..
     */
    private void initControls() {

        mView.findViewById(R.id.rl_update).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rl_update:
                Toast.makeText(getActivity(), "Profile updated successfully...", Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
                break;
        }
    }
}
